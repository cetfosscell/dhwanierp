Event Registration System version 1.0
	Developed by
		MELWIN JOSE
			Active in Facebook, Linkedin, Google+
				maxx913@gmail.com
				melwin.jose.1991@gmail.com
			CSE 2009-2013, CET

Instructions:
> Install and start wamp ( for windows )
> Unzip ERS.zip[rar] to wamp/www/ERS/
	[NOTE : if folder "registration_package"  is created inside ERS/
	                copy its contents back to ERS so that the following
	                are present in ERS :
		folders : "registration" and "results"
		files : "READ ME.txt" and "dhwani_db.sql" 
	]
> Open browser ( not IE ) and type "localhost" in address bar
> Select "Your Aliases"-->"phpmyadmin" ( open in a new tab )
> Create a new database "dhwani"
> Import the given "dhwani_db.sql" into this database
	( Its the database of Drishti'12 after day 1 )
	TABLES
	college : list of all college and their points [ college wise points disabled by default ]
	events : list of events, its winner and points for 1st,2nd & 3rd 
		[ points featured disabled ]
		[ first_pt by default used as events category ]
	grp : list of groups participation
	hospitality : not used [ disabled by default ]
	members : for admins [ not used  ]
	registration : list of registrants

> Select ERS from "Your Projects" in the "WAMPServer Homepage" tab of your browser
> Select "registration/" and Type in "melwin" as password
> Now the Event Registration Software is running in your system. You can setup a wireless ad-hoc
	network (in this system) at your event registration desk with this system as the server.
	Other systems need to connect to this network (WIFI) and type the IP address of this system
	in their browser to access the WAMPSever Home Page. Select the "ERS" from "Your Projects"
	to get the registration page at their system. Please keep a backup of the DB at regular intervals
	in this system.
> To edit the pages, open the corresponding page in any editor. Documentation provided in respective files.
