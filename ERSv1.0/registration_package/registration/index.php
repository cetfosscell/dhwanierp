<?php

	session_start();
	if(isset($_SESSION['user'])==false)
	{		
			/* 
			 *	registration desk login
			 */
			header('location: login.php');
	}			
?>

<?php

	/*
	 *  set HOST,USER,PASSWORD & DATABASE
	 */
	include('config.php');			
	
	
	/*
	 *  page title and script
	 */
	include('head.php');			
	
	
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) 
	{
		die('Failed to connect to server: ' . mysql_error());
	}
	
	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) 
	{
		die("Unable to select database");
	}
	
?>

<body>

<div id="wrapper">
	<div id="top">
	</div>
	<div id="content">
	<div id="header">
	<div id="logo">
	<img style="width: 164px;" src="images/logo.png">
	 REGISTRATION
	</div>
	</div>
	<div id="main">
	<?php 
	
			/* 
			 *  left navigation bar
			 */
			include('title.php');
			 	
	?>
	<div id="frm" style="position: relative; overflow: hidden;">
		<div id="stuff"></div>
		<form id="reg_event" action="register.php" method="post" enctype="multipart/form-data">
		
        <fieldset class="form-item">
            <legend><b>Your Details</b></legend>
				
				<div id="regi">
					NAME
                   <input type="text" id="reg_name" name="reg_name" value="" /><br/>
				 </div>
				<div id="regi">
				   COLL_ID
					<input type="text" id="col_id" name="col_id"  ><br />
				</div>
			  	<div id="regi">
				   EMAIL
					<input type="text" id="reg_email" name="reg_email" ><br />
				</div>
				<div id="regi">
					COLLEGE
					
					<select name="reg_college" id="reg_college">
					<?php 
						  /*
						   *  get college names from DB into $college[]
						   */
						  include('college.php'); 		
					?>
					<?php 
						foreach($college as $collegeget): 
					?>
					<option value="<?php  echo $collegeget['coll_name']; ?>">
									<?php echo $collegeget['coll_name']; ?>
					</option>
					<?php endforeach; ?>
					</select>
					
					<a id="col" style="color:#F68E06;">add college</a>
					<div id="ot" style="display:none;">
					<form action="coll.php" method="post" enctype="multipart/form-data">
						<input type="text" id="coll_name" name="coll_name" >
						<input type="hidden" id="ins" name="ins" value="1" >
						<input name="entry" id="entry" type="submit" value="submit">
					</form>
					</div>
				</div>
				
			   	<div id="regi">
					PHONE
                  <input type="text" id="reg_mob" name="reg_mob" >
			    </div>
				   	
        </fieldset>
		
		
		<fieldset class="the_events">
			<legend><b>Choose Events</b></legend>
			<?php 
					/*
					 *	takes events from database and groups them
					 */
					include('reg_events.php');	
			 ?>		
		</fieldset>
		
		
		<!-- This has been hidden remove the  style="display:none;"  from the next line to make it visible  -->
        <fieldset class="the_events" style="display:none;">
            <legend>Hospitality</legend>
			<label>
			<input name="reg_acc" type="checkbox" id="reg_acc" value="1" />
			Need Accomodation ?
			</label>
			<label>
               <p>
                    <label>
                     <input type="radio" id="gender_0" value="1" name="gender">
                      Male</label>
                      <label>
                      <input type="radio" id="gender_1" value="0" name="gender">
                      Female</label>
                </p>							
		     </label>
        </fieldset>
		
		 
	     <p id="reg_error" style="color:#FFFFFF;"></p>
		 
         <input class="event-but" id="event_sub" type="submit" value="Register">
				
		</form>
		</div>	
		</div>
</div>
<div id="bottom">

</div>
</div>
</body>